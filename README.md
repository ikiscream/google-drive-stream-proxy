![image](https://i.imgur.com/sWzcGrk.png)

DEMO: [https://www.useloom.com/share/6ddca300d7294788b525f6341b01de25](https://www.useloom.com/share/6ddca300d7294788b525f6341b01de25)

## Instalación

Requerimientos de php:

    apt install php-mysql php-db php-mbstring php-curl


1.- Extrae el zip dist.zip: https://gitlab.com/zodman/google-drive-stream-proxy/uploads/57f9ef76b935ae9095001fa839d692ce/dist.zip

2.- Edita el conf.php


## Development

    composer install
    yarn install
    yarn run build
